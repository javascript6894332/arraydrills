function reduce(elements, cb, startingValue){
    if (Array.isArray(elements)){
        let sum = startingValue !== undefined ? initialValue : elements[0];
        for ( let i = 1; i < elements.length; i++){
            sum = cb(sum, elements[i])
        }

        return sum;
    }
}

module.exports = reduce;
