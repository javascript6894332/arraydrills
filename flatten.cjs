function flatten(elements, cb) {
    if (Array.isArray(elements)){
      flattenInner(elements);
      
      function flattenInner(innerElements) {
        for (let i = 0; i < innerElements.length; i++) {
          if (Array.isArray(innerElements[i])) {
            flattenInner(innerElements[i]);
          } else {
            cb(innerElements[i]);
          }
        }
      }
    }
  
  }

module.exports = flatten;
