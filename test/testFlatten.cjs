const flatten = require('../flatten.cjs'); //Importing function
const items = [1, [2], [3, [[4]]]]  //define an array
const result=[] //initializing new array

flatten( items, function(item){   // invoking parent function
    result.push(item);
})

console.log(result); //printing result
