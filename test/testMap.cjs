const map = require('../map.cjs'); //Importing function
const items = [1, 2, 3, 4, 5]  //define an array
const result=[] //initializing new array
map( items, function(item){   // invoking parent function
    result.push(item*2);
})

console.log(result);  //printing result
