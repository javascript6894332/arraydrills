const filter = require('../filter.cjs'); //Importing function
const items = [1, 2, 3, 4, 5];  //define an array
const result=[]; //initializing new array
filter( items, function(item){   // invoking parent function
    if (item > 2){
        result.push(item);
    }
})

console.log(result);  //printing result
