const reduce = require('../reduce.cjs'); //Importing function
const items = [1, 2, 3, 4, 5]  //define an array

const sum = reduce( items, function(startingValue, element){   // invoking parent function
    return startingValue + element
})

//printing result
console.log(sum);
