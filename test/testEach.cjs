const each = require('../each.cjs'); //Importing function
const items = [1, 2, 3, 4, 5]  //define an array

each( items, function(item) {   // invoking parent function
    console.log(item); 
} )
