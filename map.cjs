function map(elements, cb){
    if (Array.isArray(elements)){
        for ( let index = 0; index < elements.length; index++){
            cb(elements[index]);
        }
    }
}

module.exports = map;
